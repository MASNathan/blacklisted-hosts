package main

import (
	"gopkg.in/yaml.v2"
	"io/ioutil"
)

type Configuration struct {
	Nameservers    []string            `yaml:"nameservers"`
	WhitelistFiles []string            `yaml:"whitelist_files"`
	WildcardFiles  []string            `yaml:"wildcard_files"`
	Blacklist      map[string][]string `yaml:"blacklist"`
}

func NewConfiguration(p string) (*Configuration, error) {
	c := &Configuration{}

	cf, err := ioutil.ReadFile(p)
	if err != nil {
		return nil, err
	}

	if err = yaml.Unmarshal(cf, &c); err != nil {
		return nil, err
	}

	return c, nil
}
