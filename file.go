package main

import (
	"bufio"
	"os"
	"path"
	"strings"
)

type FileManager struct {
	files     map[string]*File
	directory string
}

func NewFileManager(dir string) *FileManager {
	return &FileManager{
		files:     make(map[string]*File),
		directory: strings.TrimRight(dir, "/") + "/",
	}
}

type File struct {
	remotePath string
	localPath  string

	file    *os.File
	scanner *bufio.Scanner
}

func NewFile(p string) *File {
	if p[0:4] == "http" {
		return &File{
			remotePath: p,
			localPath:  path.Join(os.TempDir(), MD5(p)),
		}
	}

	return &File{
		localPath: p,
	}
}

func (f *File) Open() error {
	if _, err := os.Stat(f.localPath); os.IsNotExist(err) {
		if f.remotePath == "" {
			file, err := os.Create(f.localPath)
			if err != nil {
				return err
			}

			f.file = file

			return nil
		}

		if err := DownloadFile(f.localPath, f.remotePath); err != nil {
			return err
		}
	}

	file, err := os.OpenFile(f.localPath, os.O_RDWR, os.ModePerm)

	if err != nil {
		return err
	}

	f.file = file
	scanner := bufio.NewScanner(f.file)
	f.scanner = scanner

	return nil
}

func (f *File) ReadLine() *string {
	if f.scanner.Scan() == true {
		line := f.scanner.Text()

		return &line
	}

	return nil
}

func (f *File) WriteLine(ln string) error {
	_, err := f.file.WriteString(ln + "\n")
	if err != nil {
		return err
	}

	return nil
}

func (f *File) Close() error {
	return f.file.Close()
}

func (fm *FileManager) WriteLine(file string, ln string) error {
	loc := fm.directory + strings.TrimLeft(file, "/")
	f, exists := fm.files[loc]

	if exists == false {
		f = NewFile(loc)
		fm.files[loc] = f

		err := f.Open()
		if err != nil {
			return err
		}
	}

	return f.WriteLine(ln)
}

func (fm *FileManager) Close() (err error) {
	for _, f := range fm.files {
		err = f.file.Close()
	}

	return
}
