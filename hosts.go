package main

import (
	"fmt"
	"github.com/orcaman/concurrent-map"
	"sort"
	"sync"
)

type HostList struct {
	hosts *cmap.ConcurrentMap
}

type Host struct {
	Categories []string
	Exists     bool
}

func (hl *HostList) Push(h string, i *Host) {
	if hl.hosts == nil {
		m := cmap.New()
		hl.hosts = &m
	}

	hl.hosts.Set(h, i)
}

func (hl *HostList) Get(h string) *Host {
	if tmp, ok := hl.hosts.Get(h); ok {
		return tmp.(*Host)
	}

	return nil
}

func (hl *HostList) Has(h string) bool {
	if hl.hosts == nil {
		return false
	}

	return hl.hosts.Has(h)
}

func (hl *HostList) Delete(h string) {
	hl.hosts.Remove(h)
}

func (hl *HostList) Count() int {
	return hl.hosts.Count()
}

func (hl *HostList) Merge(l *HostList) {
	for h, i := range l.hosts.Items() {
		hl.hosts.Set(h, i)
	}
}

func (hl *HostList) Items() map[string]interface{} {
	return hl.hosts.Items()
}

func (hl *HostList) Load(c *Configuration) {
	wg := &sync.WaitGroup{}
	defer wg.Wait()

	for category, lists := range c.Blacklist {
		for _, list := range lists {

			wg.Add(1)
			go func(hl *HostList, category string, url string, wg *sync.WaitGroup) {
				defer wg.Done()

				err := ParseHostFile(hl, url, category)
				if err != nil {
					panic(err)
				}
			}(hl, category, list, wg)
		}
	}
}

func (hl *HostList) WriteToFiles(dir string) (err error) {
	fm := NewFileManager(dir)

	defer func() {
		e := fm.Close()

		if err == nil {
			err = e
		}
	}()

	var keys []string
	for k := range hl.Items() {
		keys = append(keys, k)
	}
	sort.Strings(keys)

	for _, domain := range keys {
		host := hl.Get(domain)

		for _, cat := range host.Categories {
			if err = fm.WriteLine(cat+".txt", fmt.Sprintf("127.0.0.1\t%s", domain)); err != nil {
				return
			}
		}

		if err = fm.WriteLine("hosts.txt", fmt.Sprintf("127.0.0.1\t%s", domain)); err != nil {
			return
		}
	}

	return
}

func (h *Host) AddCategory(c string) {
	for _, b := range h.Categories {
		if b == c {
			return
		}
	}

	h.Categories = append(h.Categories, c)
}
