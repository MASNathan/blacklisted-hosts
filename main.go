package main

import (
	"fmt"
)

func main() {
	c, err := NewConfiguration("config.yaml")
	if err != nil {
		panic(err)
	}

	hl := &HostList{}
	hl.Load(c)

	fmt.Printf("Total unique domains: %d", hl.Count())

	//TODO: add wildcards
	fmt.Printf("Total unique domains: %d", hl.Count())

	//TODO: remove whitelisted
	fmt.Printf("Total unique domains: %d", hl.Count())

	if err := hl.WriteToFiles("data/"); err != nil {
		panic(err)
	}
}
