package main

import (
	"github.com/asaskevich/govalidator"
	"strings"
)

func ParseHostFile(hl *HostList, url string, category string) error {
	f := NewFile(url)

	err := f.Open()

	if err != nil {
		return err
	}

	defer f.Close()

	for {
		line := f.ReadLine()

		if line == nil {
			break
		}

		domain := parseHostLine(*line)

		if domain == nil {
			continue
		}

		if hl.Has(*domain) {
			hl.Get(*domain).AddCategory(category)
			continue
		}

		h := &Host{
			Categories: []string{category},
		}

		hl.Push(*domain, h)
	}

	return nil
}

func parseHostLine(ln string) *string {
	ln = strings.ToLower(strings.TrimSpace(ln))

	if ln == "" {
		return nil
	}

	if strings.HasPrefix(ln, "#") {
		return nil
	}

	ln = strings.Trim(ln, ".")

	if validDomain(ln) {
		return &ln
	}

	sln := strings.Split(ln, " ")

	d := sln[len(sln)-1]

	if !validDomain(d) {
		return nil
	}

	return &d
}

func validDomain(domain string) bool {
	return !strings.Contains(domain, " ") && govalidator.IsDNSName(domain)
}
